// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDjD-2nXTCHPykM7CK-U7KSncG0JxcZu4k",
    authDomain: "budget-81f98.firebaseapp.com",
    databaseURL: "https://budget-81f98.firebaseio.com",
    projectId: "budget-81f98",
    storageBucket: "budget-81f98.appspot.com",
    messagingSenderId: "780518415574",
    appId: "1:780518415574:web:3b6fab168d2a904e393d77",
    measurementId: "G-Z7BSJKRXD6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
