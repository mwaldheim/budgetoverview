import { Component } from '@angular/core';

@Component({
  selector: 'budgetoverview-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'budget';
}
