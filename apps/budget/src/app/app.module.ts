import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {RouterModule, Routes} from "@angular/router";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {ChartsDirective} from './feature/charts.directive';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {AuthGuard} from "./core/guard/auth.guard";
import {AuthService} from "./core/services/auth.service";
import {AngularFireModule} from '@angular/fire/compat';
import {AngularFireAuthModule} from '@angular/fire/compat/auth';

const routes: Routes = [
  {path: '', redirectTo: 'portal', pathMatch: 'full'},
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'portal', loadChildren: () => import('./pages/portal/portal.module').then(m => m.PortalModule),
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [AppComponent, ChartsDirective],
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    })],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
