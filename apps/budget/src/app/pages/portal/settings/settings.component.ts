import { Component, OnInit } from '@angular/core';
import {OtherUsersDTO} from "../../../core/models/otherUsersDTO";

@Component({
  selector: 'budgetoverview-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  invitedUsers: OtherUsersDTO[] = [];
  userInvites: OtherUsersDTO[] = [];

  constructor() {}

  ngOnInit(): void {}

}
