import { Component, OnInit } from '@angular/core';
// version from package.json
import pack  from '../../../../../../package.json';
import {AuthService} from "../../core/services/auth.service";

@Component({
  selector: 'budgetoverview-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss'],
})
export class PortalComponent implements OnInit {

  version = '0.0.0';
  name = '';
  value: string = '';
  year: number = new Date().getFullYear();

  constructor(public auth: AuthService) {
    this.version = pack.version;
    this.name = pack.name;
  }

  ngOnInit(): void {}
}
