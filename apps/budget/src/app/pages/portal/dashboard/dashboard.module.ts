import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../../../shared/shared.module';
import { InputTableComponent } from './input-table/input-table.component';
import { OutputTableComponent } from './output-table/output-table.component';
import { OffsetTableComponent } from './offset-table/offset-table.component';
import { OffsetChartComponent } from './offset-chart/offset-chart.component';
import { BudgetChartComponent } from './budget-chart/budget-chart.component';
import {CoreModule} from "../../../core/core.module";
import {BookingComponent} from "./booking/booking.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";

@NgModule({
  declarations: [
    DashboardComponent,
    InputTableComponent,
    OutputTableComponent,
    OffsetTableComponent,
    OffsetChartComponent,
    BudgetChartComponent,
    BookingComponent
  ],
    imports: [CommonModule, DashboardRoutingModule, SharedModule, CoreModule],
})
export class DashboardModule {}
