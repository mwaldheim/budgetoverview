import {Component, Input, OnInit} from '@angular/core';
import {ChartsDirective} from "../../../../feature/charts.directive";
import {ApexAxisChartSeries, ApexChart, ApexGrid, ApexLegend, ApexPlotOptions} from "ng-apexcharts";

@Component({
  selector: 'budgetoverview-offset-chart',
  templateUrl: './offset-chart.component.html',
  styleUrls: ['./offset-chart.component.scss'],
})
export class OffsetChartComponent extends ChartsDirective<ApexAxisChartSeries> implements OnInit {

  @Input()
  get labels(): string[] {
    return this._labels;
  }

  set labels(value: string[]) {
    this._labels = value;
  }
  private _labels: string[] = [];


  public override chartOptionsChart: ApexChart = {
    type      : 'donut',
    fontFamily: 'inherit',
    foreColor : 'inherit',
    animations: {
      enabled: true
    },
    zoom      : {
      enabled: false
    },
  }

  public chartPlotOptions: ApexPlotOptions = {
    pie: {
      expandOnClick: false,
      startAngle   : -90,
      endAngle     : 90,
      offsetY      : 40,
      donut        : {
        labels: {
          show : true,
          total: {
            show : true,
            label: 'Total',
            color: '#000',
            formatter(w) {
              return `${w.globals.seriesTotals.reduce((a: any, b: any) => {
                return a + b;
              }, 0)}`;
            }
          }
        }
      }
    }
  }

  public chartGrid: ApexGrid = {
    padding: {
      bottom: -120
    }
  }
  chartOptionsLegend: ApexLegend = {
    position: 'bottom'
  };

  public override updateSeries() {
    if (this.data) {
      this.chartOptionSeries = this.data;
    }
  }
}
