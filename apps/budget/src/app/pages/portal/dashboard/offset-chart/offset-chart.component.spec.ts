import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffsetChartComponent } from './offset-chart.component';

describe('OffsetChartComponent', () => {
  let component: OffsetChartComponent;
  let fixture: ComponentFixture<OffsetChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OffsetChartComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(OffsetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
