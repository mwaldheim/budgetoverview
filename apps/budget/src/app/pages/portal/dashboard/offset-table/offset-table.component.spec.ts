import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffsetTableComponent } from './offset-table.component';

describe('OffsetTableComponent', () => {
  let component: OffsetTableComponent;
  let fixture: ComponentFixture<OffsetTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OffsetTableComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(OffsetTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
