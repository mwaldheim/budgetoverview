import { Component, OnInit } from '@angular/core';
import {ApexAxisChartSeries} from "ng-apexcharts";
import {MatDialog} from "@angular/material/dialog";
import {BookingComponent} from "./booking/booking.component";

@Component({
  selector: 'budgetoverview-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  budgetLabels: string[] = ['Ausgaben', 'Freies Budget'];
  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}
  getBudget(): ApexAxisChartSeries | undefined {
    return undefined;
  }

  openDialog() {
    const dialogRef = this.dialog.open(BookingComponent,
      {
        width: '35rem',
        maxWidth: '80%'
      });

    const subscribeSave = dialogRef.afterClosed().subscribe(result => {

    })
  }
}
