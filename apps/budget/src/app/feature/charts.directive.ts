import {AfterViewInit, Directive, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ChartComponent
} from 'ng-apexcharts';
import {ChartsService} from '../core/services/charts.service';

@Directive({
  selector: '[budgetoverviewCharts]'
})
export class ChartsDirective<ChartType> implements OnInit, AfterViewInit, OnChanges {


  @Input() data: ApexAxisChartSeries | undefined;


  public chartOptionSeries: ApexAxisChartSeries = [{data: []}];

  public chartOptionsXaxis: ApexXAxis = {
    categories: []
  }

  public chartOptionsChart: ApexChart = {
    type: 'area',
    height: 350,
    fontFamily: 'inherit',
    foreColor: 'inherit',
    animations: {
      enabled: true
    },
    zoom: {
      enabled: false
    },
  }
  public chartOptionsTitle: ApexTitleSubtitle = {
    text: ''
  }
  public chartOptionTooltip: ApexTooltip = {
    enabled: true
  }
  public chartOptionStroke: ApexStroke = {
    curve: 'smooth',
    width: 2
  }

  public chartOptionDataLabels: ApexDataLabels = {
    enabled: false
  }

  constructor(public srvChart: ChartsService<ChartType>) {

  }

  ngOnInit(): void {
    this.updateSeries();
  }

  public updateSeries() {
    if (this.data) {
      this.chartOptionSeries = this.srvChart.randomizeArray(this.data);
    }
  }

  ngAfterViewInit(): void {
    this.updateSeries();
  }

  ngOnChanges(): void {
    this.updateSeries();
  }


}
