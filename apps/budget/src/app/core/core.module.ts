import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BudgetsService} from "./services/budgets.service";
import {ChartsService} from "./services/charts.service";



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    BudgetsService,
    ChartsService
  ]
})
export class CoreModule { }
