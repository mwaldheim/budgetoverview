export interface OtherUsersDTO {
  name: string;
  inviteDate: number;
}
