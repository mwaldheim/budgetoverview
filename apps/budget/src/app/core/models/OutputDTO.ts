export interface OutputDTO {
  name: string;
  amount: number;
}
