export interface InputDTO {
  name: string;
  amount: number;
}
