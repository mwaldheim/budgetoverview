export interface OffsetDTO {
  name: string;
  amount: number;
  payoff: number;
  payOffMonths: number;
}
