import {Injectable, NgZone} from '@angular/core';
import * as auth from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import {UserDTO} from "../models/UserDTO";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any|UserDTO;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        let item = localStorage.getItem("user");
        if (item) {
          JSON.parse(item);
        }
      } else {
        localStorage.removeItem("user");
        let item = localStorage.getItem("user");
        if (item) {
          JSON.parse(item);
        }
      }
    });
  }


  get isLoggedIn(): boolean {
    let user = localStorage.getItem('user');
    user = user ? JSON.parse(user) : null;
    // @ts-ignore
    return (user !== null && user.emailVerified !== false);
  }

  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider()).then((res: any) => {
      if (res) {
        this.router.navigate(['portal/dashboard']);
      }
    });
  }

  AuthLogin(provider: any) {
    return this.afAuth.signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['portal/dashboard']);
        });
        this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error)
      });
  }

  SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: UserDTO = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }
  // Sign out
  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }}
