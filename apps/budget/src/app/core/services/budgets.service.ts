import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {InputDTO} from "../models/InputDTO";
import {OutputDTO} from "../models/OutputDTO";
import {OffsetDTO} from "../models/OffsetDTO";
import {ApexAxisChartSeries} from "ng-apexcharts";

@Injectable({
  providedIn: 'root'
})
export class BudgetsService {

  inputData: BehaviorSubject<InputDTO[]> = new BehaviorSubject<InputDTO[]>([]);
  outputData: BehaviorSubject<OutputDTO[]> = new BehaviorSubject<OutputDTO[]>([]);


  offsetData: BehaviorSubject<OffsetDTO[]> = new BehaviorSubject<OffsetDTO[]>([]);

  constructor() { }

  getBudget(): ApexAxisChartSeries {
    return [];
  }

  getOffsetTimesheet(): ApexAxisChartSeries {
    return [];
  }
}
