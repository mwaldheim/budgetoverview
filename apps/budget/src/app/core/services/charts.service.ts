import { Injectable } from '@angular/core';
import { ApexAxisChartSeries } from 'ng-apexcharts';

@Injectable({
  providedIn: 'root'
})
export class ChartsService<ChartType> {

  randomizeArray(chartData: ApexAxisChartSeries) {
    // shuffel the array and return it
    return chartData.map((data: any) => {
      data.data = data.data.sort(() => Math.random() - 0.5);
      return data;
    });
  }

}
