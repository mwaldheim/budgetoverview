import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import localeDe from '@angular/common/locales/de';
import {RouterModule, RouterOutlet} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldModule,
} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatChipsModule} from '@angular/material/chips';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule} from '@angular/common/http';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MenuComponent} from './menu/menu.component';
import {CardComponent} from './card/card.component';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {NgApexchartsModule} from 'ng-apexcharts';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatAccordion, MatExpansionModule} from '@angular/material/expansion';
import {EmptyComponent} from './empty/empty.component';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialog,
  MatDialogModule,
} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {
  ErrorStateMatcher,
  MAT_DATE_LOCALE,
  MatNativeDateModule,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material/core';
import {AvatarComponent} from './avatar/avatar.component';
import {MatTooltipModule} from "@angular/material/tooltip";

const COMP_IMPORT_EXPORT = [MenuComponent, CardComponent, EmptyComponent, AvatarComponent];

const COMP_IMPORT = [];
const MODULE_IMPORT_EXPORT = [
  CommonModule,
  RouterOutlet,
  FlexModule,
  NgApexchartsModule,
  HttpClientModule,
  ExtendedModule,
  MatListModule,
  MatIconModule,
  RouterModule,
  MatButtonModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatChipsModule,
  MatFormFieldModule,
  MatSelectModule,
  MatToolbarModule,
  MatSlideToggleModule,
  MatCardModule,
  MatMenuModule,
  MatExpansionModule,
  MatInputModule,
  MatDialogModule,
  MatRadioModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTooltipModule
];

const MODUL_IMPORT = [];

registerLocaleData(localeDe);

@NgModule({
  declarations: [...COMP_IMPORT_EXPORT],
  imports: [...MODULE_IMPORT_EXPORT],
  exports: [...MODULE_IMPORT_EXPORT, ...COMP_IMPORT_EXPORT],
  providers: [
    {provide: LOCALE_ID, useValue: 'de'},
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {appearance: 'outline'},
    },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    MatDatepickerModule,
  ],
})
export class SharedModule {
}
