## [1.1.0](https://gitlab.com/mwaldheim/budgetoverview/compare/v1.0.0...v1.1.0) (2022-09-05)


### :sparkles: Features

* Add Auth ([c5ec228](https://gitlab.com/mwaldheim/budgetoverview/commit/c5ec228eda0d6ef1199b8cf461c8652aed8adc73))
* Add complete mask of portal incl. settings ([8dee613](https://gitlab.com/mwaldheim/budgetoverview/commit/8dee613a74f2362ae2a9f9f954b667449957e3ff))


### :repeat: Build System

* Optimize Build-Size of Component ([d2343e1](https://gitlab.com/mwaldheim/budgetoverview/commit/d2343e1fcb38e464871c8ea3fd870df66dd55293))

## 1.0.0 (2022-09-01)


### :sparkles: Features

* Init App for Budget ([7dc30fb](https://gitlab.com/mwaldheim/budgetoverview/commit/7dc30fb191cd34b4a5561b4e454ca1ed48ea630c))


### :bug: Fixes

* Add Basic Commands ([1883248](https://gitlab.com/mwaldheim/budgetoverview/commit/188324887fbe57f7cfa617a53ace849eed31113a))
* Remove Cypress ([f3b90db](https://gitlab.com/mwaldheim/budgetoverview/commit/f3b90db6b69988c87c5388b7a9d2c54548243420))
